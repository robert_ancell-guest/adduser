#! /usr/bin/perl -Idebian/tests/lib

# check enforcement of basic username validity

use diagnostics;
use strict;
use warnings;

use AdduserTestsCommon;

my @invalid_names = ("12345");

foreach (@invalid_names) {
  my @cmd = ('/usr/sbin/adduser', '--quiet', 
    '--force-badname', '--ingroup', 'nogroup', 
    '--gecos', '""', '--disabled-password', $_,
    '2>&1');
  my $cmdline = join(' ', @cmd);
  my $cmdout = `$cmdline`;
  my $cmdret = $?;

  isnt($cmdret, 0, "command failure (expected): $cmdline");
  like($cmdout, qr/digits/, "error message is correct");

  assert_user_does_not_exist($_);
}

